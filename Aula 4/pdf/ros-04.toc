\beamer@endinputifotherversion {3.24pt}
\select@language {brazil}
\select@language {brazil}
\beamer@sectionintoc {1}{O que \IeC {\'e} uma m\IeC {\'a}quina de estados?}{3}{0}{1}
\beamer@sectionintoc {2}{SMACH}{5}{0}{2}
\beamer@subsectionintoc {2}{1}{Uso do SMACH}{5}{0}{2}
\beamer@subsectionintoc {2}{2}{Instalando o smach no ROS}{6}{0}{2}
\beamer@subsectionintoc {2}{3}{Tipos de m\IeC {\'a}quina de estados}{7}{0}{2}
\beamer@subsectionintoc {2}{4}{Codificando uma m\IeC {\'a}quina de estados simples}{12}{0}{2}
\beamer@sectionintoc {3}{Exerc\IeC {\'\i }cio}{15}{0}{3}
\beamer@sectionintoc {4}{Refer\IeC {\^e}ncias}{16}{0}{4}
