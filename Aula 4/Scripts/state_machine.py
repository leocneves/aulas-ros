#!/usr/bin/env python

import roslib;
import rospy
import smach
import smach_ros
import time
from geometry_msgs.msg import Twist

# define state 1
class State1(smach.State):
    def __init__(self):
        smach.State.__init__(self, outcomes=['Success','Fail'])
        self.counter = 0

    def execute(self, userdata):
        rospy.loginfo('Executing state 1')
        if self.counter < 10:
            self.counter += 1
            time.sleep(1)
            return 'Success'
        else:
            return 'Fail'


# define state 2
class State2(smach.State):
    def __init__(self):
        smach.State.__init__(self, outcomes=['Success'])

    def execute(self, userdata):
        rospy.loginfo('Executing state 2')
        time.sleep(1)
        vel.linear.x = 0.2
        vel.angular.z = 0.0
        pub.publish(vel)
        return 'Success'

# define state 3
class State3(smach.State):
    def __init__(self):
        smach.State.__init__(self, outcomes=['Success'])

    def execute(self, userdata):
        rospy.loginfo('Executing state 3')
        time.sleep(1)
        vel.linear.x = 0.0
        vel.angular.z = 0.2
        pub.publish(vel)
        return 'Success'





def main():
    rospy.init_node('smach_example_state_machine')

    vel = Twist()
    pub = rospy.Publisher('/turtle1/cmd_vel', Twist, queue_size=10)

    # Create a SMACH state machine
    sm = smach.StateMachine(outcomes=['outcome4'])

    # Open the container
    with sm:
        # Add states to the container
        smach.StateMachine.add('STATE_1', State1(),
                               transitions={'Success':'STATE_2', 'Fail':'outcome4'})
        smach.StateMachine.add('STATE_2', State2(),
                               transitions={'Success':'STATE_3'})
        smach.StateMachine.add('STATE_3', State3(),
                               transitions={'Success':'STATE_2'})

    # Create and start the introspection server
    sis = smach_ros.IntrospectionServer('server_name', sm, '/EXERCICIO')
    sis.start()
    # Execute SMACH plan
    outcome = sm.execute()
    # Wait for ctrl-c to stop the application
    rospy.spin()
    sis.stop()


if __name__ == '__main__':

    main()