#!/usr/bin/env python
import rospy
from geometry_msgs.msg import Twist
def turtle_circle():
 pub = rospy.Publisher('/turtle1/cmd_vel', Twist, queue_size=10)
 rospy.init_node('turtle_circle', anonymous=True)
 rate = rospy.Rate(10) # 10hz
 vel = Twist()
 while not rospy.is_shutdown():
  vel.linear.x = 0.5
  vel.angular.z = 0.2
  pub.publish(vel)
  rate.sleep()
if __name__ == '__main__':
 try:
  turtle_circle()
 except rospy.ROSInterruptException:
  pass
