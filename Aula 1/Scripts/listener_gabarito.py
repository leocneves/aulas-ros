#!/usr/bin/env python
import rospy
from geometry_msgs.msg import Twist
from turtlesim.msg import Pose

def callback(data):
 print data.x

def turtle_x():
 rospy.init_node('turtle_x', anonymous=True)
 rospy.Subscriber('/turtle1/pose', Pose, callback)

 pub = rospy.Publisher('/turtle1/cmd_vel', Twist, queue_size=10)
 rate = rospy.Rate(10) # 10hz
 vel = Twist()

 rospy.spin()
if __name__ == '__main__':
 turtle_x()
