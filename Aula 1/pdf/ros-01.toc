\beamer@sectionintoc {1}{O que \IeC {\'e} ROS?}{3}{0}{1}
\beamer@subsectionintoc {1}{1}{Desafios da rob\IeC {\'o}tica}{3}{0}{1}
\beamer@subsectionintoc {1}{2}{O que \IeC {\'e} ROS?}{5}{0}{1}
\beamer@sectionintoc {2}{Hist\IeC {\'o}ria}{6}{0}{2}
\beamer@sectionintoc {3}{Como funciona?}{9}{0}{3}
\beamer@subsectionintoc {3}{1}{Estrutura}{9}{0}{3}
\beamer@subsectionintoc {3}{2}{Publisher e Subscriber}{10}{0}{3}
\beamer@sectionintoc {4}{M\IeC {\~a}os a obra}{12}{0}{4}
\beamer@subsectionintoc {4}{1}{Sistema Operacional}{12}{0}{4}
\beamer@subsectionintoc {4}{2}{Configurando o workspace do ROS}{18}{0}{4}
\beamer@subsectionintoc {4}{3}{Criando e navegando em um package}{21}{0}{4}
\beamer@subsectionintoc {4}{4}{Criando publishers e subscribers}{24}{0}{4}
