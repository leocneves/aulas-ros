#!/usr/bin/env python

import rospy
from std_msgs.msg import MENSAGEM


pub = rospy.Publisher('TOPICO', MENSAGEM, queue_size=10)

def callback(data):


def main():
    rospy.init_node('Node', anonymous=True)
    rospy.Subscriber('TOPICO', MENSAGEM, callback).
    # Initial movement.
    pub.publish(MENSAGEM)
    rospy.spin()


if __name__ == '__main__':
    try:
        main()
    except rospy.ROSInterruptException:
        pass