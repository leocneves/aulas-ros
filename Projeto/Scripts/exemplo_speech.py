#!/usr/bin/env python
import rospy
from std_msgs.msg import String, Float64
from dragonfly_speech_recognition.srv import GetSpeech
from dragonfly_speech_recognition.msg import Choice

# def callback(data):


def main():

    spec = ['Robot','Yes','No']
    spec = '|'.join(spec)
    choices =('id','values','0','0')
    time_out = GetSpeech()
    time_out.secs = 100
    time_out.nsecs = 0

    rospy.init_node('Node', anonymous=True)

    # pub = rospy.Publisher('TOPICO', MENSAGEM, queue_size=10)

    # rospy.Subscriber('TOPICO', MENSAGEM, callback)

    rospy.wait_for_service('speech_client/get_speech')
    recognized = rospy.ServiceProxy('speech_client/get_speech', GetSpeech)
    # Initial movement.
    # pub.publish(MENSAGEM)
    try:
              result = recognized(spec=spec, time_out=time_out)
              rospy.loginfo(result.result)
    except rospy.ServiceException as exc:
              print("Service did not process request: " + str(exc))
    # rospy.spin()

if __name__ == '__main__':
    try:
        main()
    except rospy.ROSInterruptException:
        pass