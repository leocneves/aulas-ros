#!/usr/bin/env python
import rospy
import cv2
import cv2.cv
import numpy
from sensor_msgs.msg import Image
from cv_bridge import CvBridge

def nothing(x):
    pass

def processImage(msgImg):
    bridge = CvBridge()
    
    img = bridge.imgmsg_to_cv2(msgImg, 'bgr8')
    hsv_img = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)

    (linhas, colunas, canais) = img.shape
    px = colunas / 2
    py = linhas / 2
    
    '''
        Preencher os valores h_alto e h_baixo com os seguintes valores para:
        |-----------------------------------------------------------------|
        |         | azul | amarelo | laranja | verde | vermelho | violeta |
        |-----------------------------------------------------------------|
        | h_alto  | 130  |   38    |    22   |   61  |    179   |   160   |
        |-----------------------------------------------------------------|
        | h_baixo |  75  |   22    |     0   |   21  |    160   |   130   |
        |-----------------------------------------------------------------|
    '''
    h_alto = 130
    h_baixo = 75
    s_alto = 255 
    s_baixo = 130
    v_alto = 255
    v_baixo = 0

    baixo = numpy.array([h_baixo,s_baixo,v_baixo])
    alto = numpy.array([h_alto,s_alto,v_alto])

    mascara = cv2.inRange(hsv_img, baixo, alto)

    kernel = numpy.ones((5,5), numpy.uint8)
    erosao = cv2.erode(mascara,kernel,iterations=3)

    dilatacao = cv2.dilate(erosao, kernel, iterations=3)

    img_median = cv2.medianBlur(dilatacao,21)

    momento = cv2.moments(img_median)
    try:
        px = int(momento['m10']/momento['m00'])
        py = int(momento['m01']/momento['m00'])
        area = momento['m00']
        rospy.loginfo('p(x): %d, p(y): %d, area: %.2f', px, py, area)
    except ZeroDivisionError:
        pass

    cv2.circle(img_median,(px, py), 3, (171,110,0), 2)

    cv2.imshow('Image', img)
    cv2.imshow('Segmentacao', img_median)

    cv2.waitKey(3)
    
def initializer():
    rospy.init_node('color_segmentation', anonymous=True)
    rospy.Subscriber('/image_raw', Image, processImage)
    rospy.spin()
    cv2.destroyAllWindows()

if __name__ == "__main__":
    initializer()