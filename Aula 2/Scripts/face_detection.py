#!/usr/bin/env python
import rospy
import cv2
import cv2.cv
import numpy
from sensor_msgs.msg import Image
from cv_bridge import CvBridge
###################################################
from geometry_msgs.msg import MENSAGEM
###################################################

def processImage(msgImg):
    bridge = CvBridge()
    vel = Twist()
####################################################
    pub = rospy.Publisher('TOPICO', MENSAGEMt, queue_size=10)
####################################################
    img = bridge.imgmsg_to_cv2(msgImg, 'bgr8')
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

    face_cascade = cv2.CascadeClassifier('/usr/local/share/OpenCV/haarcascades/haarcascade_frontalface_default.xml')

    faces = face_cascade.detectMultiScale(
        gray,
        scaleFactor=1.1,
        minNeighbors=10,
        minSize=(10,10),
        flags=cv2.cv.CV_HAAR_SCALE_IMAGE
    )

    for (x, y, w, h) in faces:
        x1 = x + int(w*.1)
        x2 = x1 + int(w*.8)

        y1 = y + int(h*.2)
        y2 = y1 + int(h*.8)

        cv2.rectangle(img, (x1,y1), (x2,y2), (0,255,0), 2)

    cv2.imshow('Image', img)
    cv2.waitKey(3)

##############################################
    if CONDIÇÃO:

    else:

##############################################

def initializer():
    rospy.init_node('face_detection', anonymous=True)
    rospy.Subscriber('/image_raw', Image, processImage)


    rospy.spin()
    cv2.destroyAllWindows()

if __name__ == "__main__":
    initializer()