#!/usr/bin/env python
import rospy
import numpy
import math
import tf
from sensor_msgs.msg import Image
from cv_bridge import CvBridge
###################################################
from geometry_msgs.msg import MENSAGEM
###################################################

def distance_control():
    rospy.init_node('distance_control', anonymous=True)
    listener = tf.TransformListener()
    vel = Twist()
####################################################
    pub = rospy.Publisher('TOPICO', MENSAGEMt, queue_size=10)
####################################################
    rate = rospy.Rate(10.0)
    while not rospy.is_shutdown():
        x, y, z, distance = [0, 0, 0, 0]
        try:
            is_listenning = listener.canTransform('/openni', '/torso_1', rospy.Time(0))
            if is_listenning:
                (trans, rot) = listener.lookupTransform('/openni', '/torso_1', rospy.Time(0))
                x = trans[0]
                y = trans[1]
                z = trans[2]
                distance = numpy.linalg.norm(trans)

            if distance > 0.7 and distance < 1.7:
                if y > 0.2:
                    rospy.loginfo('left <<<<<<<<')
                elif y < -0.2:
                    rospy.loginfo('right >>>>>>>>')

            if distance > 0.0:
                rospy.loginfo('Distancia: %.4f' % distance)
            ##############################################
                if CONDIÇÃO:

                else:

            ##############################################

        except (tf.LookupException, tf.ConnectivityException, tf.ExtrapolationException), e:
            print e
            continue

        rate.sleep()

if __name__ == "__main__":
    distance_control()