\beamer@endinputifotherversion {3.24pt}
\select@language {brazil}
\select@language {brazil}
\beamer@sectionintoc {1}{Utilizando Webcam e OpenCV}{3}{0}{1}
\beamer@subsectionintoc {1}{1}{Segmenta\IeC {\c c}\IeC {\~a}o de Cor}{3}{0}{1}
\beamer@subsectionintoc {1}{2}{Detec\IeC {\c c}\IeC {\~a}o Facial}{13}{0}{1}
\beamer@sectionintoc {2}{Exerc\IeC {\'\i }cio}{18}{0}{2}
\beamer@sectionintoc {3}{Utilizando Kinect}{19}{0}{3}
\beamer@subsectionintoc {3}{1}{Controlando a Dist\IeC {\^a}ncia}{19}{0}{3}
\beamer@sectionintoc {4}{Exerc\IeC {\'\i }cio}{26}{0}{4}
\beamer@sectionintoc {5}{Ap\IeC {\^e}ndice}{27}{0}{5}
\beamer@subsectionintoc {5}{1}{Instala\IeC {\c c}\IeC {\~a}o do OpenCV}{27}{0}{5}
\beamer@subsectionintoc {5}{2}{Instala\IeC {\c c}\IeC {\~a}o do Openni}{30}{0}{5}
\beamer@sectionintoc {6}{Refer\IeC {\^e}ncias}{31}{0}{6}
