\beamer@endinputifotherversion {3.36pt}
\select@language {brazil}
\select@language {brazil}
\beamer@sectionintoc {1}{Sensor laser}{3}{0}{1}
\beamer@sectionintoc {2}{Navega\IeC {\c c}\IeC {\~a}o}{4}{0}{2}
\beamer@sectionintoc {3}{Par\IeC {\^a}metros}{5}{0}{3}
\beamer@subsectionintoc {3}{1}{Odometria}{6}{0}{3}
\beamer@subsectionintoc {3}{2}{Transformada}{7}{0}{3}
\beamer@sectionintoc {4}{T\IeC {\'o}picos do rob\IeC {\^o}}{10}{0}{4}
\beamer@sectionintoc {5}{Costmap}{11}{0}{5}
\beamer@subsectionintoc {5}{1}{Common params}{14}{0}{5}
\beamer@subsectionintoc {5}{2}{Global costmap}{15}{0}{5}
\beamer@subsectionintoc {5}{3}{Local costmap}{16}{0}{5}
\beamer@subsectionintoc {5}{4}{Base local planner}{17}{0}{5}
\beamer@sectionintoc {6}{Exerc\IeC {\'\i }cio}{19}{0}{6}
