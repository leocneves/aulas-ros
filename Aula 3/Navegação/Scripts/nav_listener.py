#!/usr/bin/env python

import rospy
import actionlib
from move_base_msgs.msg import MoveBaseAction, MoveBaseGoal
from math import radians, degrees
from actionlib_msgs.msg import *
from geometry_msgs.msg import Pose
from std_msgs.msg import Bool
import time

# from sound_play.libsoundplay import SoundClient

class map_navigation:
  def __init__(self):

    self.goalReached = False

    # initiliaze
    rospy.init_node('map_navigation', anonymous=True)
    rospy.Subscriber("nav_coordinates", Pose, self.nav_callback)

    self.nav_state = rospy.Publisher("/nav_state", Bool, queue_size=10)

    self.response = Bool()
    self.response.data = False

  def nav_callback(self, data):
      print data
      x = data.position.x
      y = data.position.y
      z = data.orientation.z
      w = data.orientation.w
      self.goalReached = self.moveToGoal(x, y, z, w)

      if (self.goalReached):
          rospy.loginfo("Congratulations!!!!")
          self.response.data = True
          self.nav_state.publish(self.response.data)
          time.sleep(1)
          self.nav_state.publish(self.response.data)
          #rospy.spin()
      else:
          rospy.loginfo("Hard Luck!")
          self.response.data = False
          self.nav_state.publish(self.response.data)
          time.sleep(1)
          self.nav_state.publish(self.response.data)

  def shutdown(self):
      # stop turtlebot
      rospy.loginfo("Quit program")
      rospy.sleep()

  def moveToGoal(self,xPose,yPose,zOri,wOri):

      #define a client for to send goal requests to the move_base server through a SimpleActionClient
      ac = actionlib.SimpleActionClient("move_base", MoveBaseAction)

      #wait for the action server to come up
      while(not ac.wait_for_server(rospy.Duration.from_sec(5.0))):
              rospy.loginfo("Waiting for the move_base action server to come up")


      goal = MoveBaseGoal()

      #set up the frame parameters
      goal.target_pose.header.frame_id = "map"
      goal.target_pose.header.stamp = rospy.Time.now()

      # moving towards the goal*/

      goal.target_pose.pose.position.x =  xPose
      goal.target_pose.pose.position.y =  yPose
      goal.target_pose.pose.position.z =  0.0
      goal.target_pose.pose.orientation.x = 0.0
      goal.target_pose.pose.orientation.y = 0.0
      goal.target_pose.pose.orientation.z = zOri
      goal.target_pose.pose.orientation.w = wOri

      rospy.loginfo("Sending goal location ...")
      ac.send_goal(goal)

      ac.wait_for_result(rospy.Duration(400))

      if(ac.get_state() ==  GoalStatus.SUCCEEDED):
          rospy.loginfo("You have reached the destinatioooon")
          return True

      else:
          return False

if __name__ == '__main__':
    try:

        # rospy.loginfo("You have reached the destination")
        map_navigation()
        rospy.spin()

    except rospy.ROSInterruptException:
        rospy.loginfo("map_navigation node terminated.")
