#!/usr/bin/env python

import rospy
from std_msgs.msg import String

from sound_play.libsoundplay import SoundClient


class voice_play:

    def __init__(self):
        self.voice = rospy.get_param("~voice", "voice_cmu_us_clb_arctic_clunits")
        self.sound_files_dir = rospy.get_param("~sound_dir", "")

        self.sound_handle = SoundClient()

        rospy.sleep(1)
        rospy.loginfo("Ready to speak...")
        rospy.Subscriber('/speech', String, self.talkback)
        rospy.Subscriber('/sounds', String, self.play_sound)
        # self.sound_handle.say('Greetings, I am Judith.', self.voice)

    def talkback(self, msg):
        strhelp = msg.data
        rospy.loginfo(msg.data)
        self.sound_handle.stopAll()
        self.sound_handle.say(strhelp, self.voice)
        rospy.sleep(0.01)

    def play_sound(self, msg):
        rospy.loginfo(msg.data)
        self.sound_handle.stopAll()
        strhelp = msg.data
        self.sound_handle.playWave(self.sound_files_dir + strhelp)
        rospy.sleep(0.01)

if __name__ == '__main__':
    rospy.init_node('voice_play', anonymous=True)
    voice_play()
    try:
        rospy.spin()
    except KeyboardInterrupt:
        print("Shutting down")