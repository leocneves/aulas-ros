#!/usr/bin/env python
import rospy
from std_msgs.msg import String
from sound_play.msg import SoundRequest
import time
import pygame
class speech_recognition:

    def __init__(self):
    	self.times = rospy.get_param('Seconds')
        self.awaiting_command = False 
        self.confirm = False
        self.chosen_time = None
        self.robot_name = 'robot'
        #self.data
        ###########################################################################
        self.sc_sub = rospy.Subscriber( topico, tipo, self.callback)
        ###########################################################################

        ##################################################################
        self.speech_pub = rospy.Publisher( topico, tipo , queue_size=20) 
        ##################################################################
        rospy.loginfo("Ready to listen...")

    def callback(self,msg):
    	speech = msg.data
    	if not self.awaiting_command and speech.lower() == self.robot_name:
    		self.awaiting_command = True
    		###########################################################
    		self.speech_pub.publish("Hi user, I am ROBOFEI @ Home Robot")
    		###########################################################
    		time.sleep(2.5)
    	elif self.awaiting_command and not self.confirm:
    		for Word in self.times:
    			##############################################
    			if (Condicao de reconhecimento da palavra):
    			##############################################
    				self.chosen_time = Word
    		print ('Chosen Time = %s seconds' % self. chosen_time)
    		if self.chosen_time != None:
    			self.confirm = True
    		else:
    			self.speech_pub.publish("Could you please repeat")
    			self.confirm = False
    		time.sleep(1)
    	elif self.confirm:
    		self.speech_pub.publish("Congratulations, you did it!")
    		time.sleep(2.5)
    		self.Success()
    		self.confirm = False
    		self.awaiting_command = False

    def Success(self):
    	pygame.mixer.pre_init(44100, 16, 2, 4096)
        pygame.init()
        pygame.mixer.music.load("/home/thiago/AM/1.mp3")
        pygame.mixer.music.play()
        time.sleep(10)
        pygame.mixer.music.stop()

if __name__ == '__main__':
    rospy.init_node('speech_recognition', anonymous=True)
    speech_recognition()
    try:
        rospy.spin()
    except KeyboardInterrupt:
        print("Shutting down")
